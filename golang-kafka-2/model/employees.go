package model

import (
	"encoding/json"
	"fmt"
)

type Employees struct {
	Id            int    `bson:"e_id" json:"id"`
	Email_address string `json:"email_address" bson:"email_address"`
	First_name    string `json:"first_name" bson:"first_name"`
	Last_name     string `json:"last_name" bson:"last_name"`
}


func SaveMessage(msg string){
	var save Employees
	json.Unmarshal([]byte(msg), &save)
	fmt.Printf("%d : %s : %s : %s\n", save.Id, save.Email_address, save.First_name, save.Last_name)
}
