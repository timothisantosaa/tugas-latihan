package main

import (
	"fmt"
	consumer2 "golang-kafka-2/consumer"
)

func main()  {
	consumer, err := consumer2.NewConsumer()
	if err != nil {
		fmt.Println("Could not create consumer: ", err)
	}

	consumer2.Consume("test-topic-1", consumer)
}
