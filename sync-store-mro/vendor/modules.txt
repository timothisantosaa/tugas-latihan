# git.alfamikro.com/go/toolkit v0.11.0
## explicit
git.alfamikro.com/go/toolkit/db
git.alfamikro.com/go/toolkit/db/mongokit
git.alfamikro.com/go/toolkit/log
git.alfamikro.com/go/toolkit/runtimekit
git.alfamikro.com/go/toolkit/springcloud
git.alfamikro.com/go/toolkit/web/httpclient
# github.com/HereMobilityDevelopers/mediary v1.0.0
github.com/HereMobilityDevelopers/mediary
# github.com/armon/go-radix v1.0.0
github.com/armon/go-radix
# github.com/aws/aws-sdk-go v1.38.20
github.com/aws/aws-sdk-go/aws
github.com/aws/aws-sdk-go/aws/awserr
github.com/aws/aws-sdk-go/aws/awsutil
github.com/aws/aws-sdk-go/aws/client/metadata
github.com/aws/aws-sdk-go/aws/credentials
github.com/aws/aws-sdk-go/aws/endpoints
github.com/aws/aws-sdk-go/aws/request
github.com/aws/aws-sdk-go/aws/signer/v4
github.com/aws/aws-sdk-go/internal/context
github.com/aws/aws-sdk-go/internal/ini
github.com/aws/aws-sdk-go/internal/sdkio
github.com/aws/aws-sdk-go/internal/sdkmath
github.com/aws/aws-sdk-go/internal/shareddefaults
github.com/aws/aws-sdk-go/internal/strings
github.com/aws/aws-sdk-go/internal/sync/singleflight
github.com/aws/aws-sdk-go/private/protocol
github.com/aws/aws-sdk-go/private/protocol/rest
# github.com/elastic/go-sysinfo v1.6.0
github.com/elastic/go-sysinfo
github.com/elastic/go-sysinfo/internal/registry
github.com/elastic/go-sysinfo/providers/aix
github.com/elastic/go-sysinfo/providers/darwin
github.com/elastic/go-sysinfo/providers/linux
github.com/elastic/go-sysinfo/providers/shared
github.com/elastic/go-sysinfo/providers/windows
github.com/elastic/go-sysinfo/types
# github.com/elastic/go-windows v1.0.1
github.com/elastic/go-windows
# github.com/fsnotify/fsnotify v1.4.9
github.com/fsnotify/fsnotify
# github.com/go-stack/stack v1.8.0
github.com/go-stack/stack
# github.com/golang/snappy v0.0.3
github.com/golang/snappy
# github.com/hashicorp/hcl v1.0.0
github.com/hashicorp/hcl
github.com/hashicorp/hcl/hcl/ast
github.com/hashicorp/hcl/hcl/parser
github.com/hashicorp/hcl/hcl/printer
github.com/hashicorp/hcl/hcl/scanner
github.com/hashicorp/hcl/hcl/strconv
github.com/hashicorp/hcl/hcl/token
github.com/hashicorp/hcl/json/parser
github.com/hashicorp/hcl/json/scanner
github.com/hashicorp/hcl/json/token
# github.com/iancoleman/strcase v0.1.3
github.com/iancoleman/strcase
# github.com/jmespath/go-jmespath v0.4.0
github.com/jmespath/go-jmespath
# github.com/joeshaw/multierror v0.0.0-20140124173710-69b34d4ec901
github.com/joeshaw/multierror
# github.com/kelseyhightower/envconfig v1.4.0
github.com/kelseyhightower/envconfig
# github.com/klauspost/compress v1.12.1
github.com/klauspost/compress/fse
github.com/klauspost/compress/huff0
github.com/klauspost/compress/zstd
github.com/klauspost/compress/zstd/internal/xxhash
# github.com/magiconair/properties v1.8.5
github.com/magiconair/properties
# github.com/mitchellh/mapstructure v1.4.1
github.com/mitchellh/mapstructure
# github.com/pelletier/go-toml v1.9.0
github.com/pelletier/go-toml
# github.com/pkg/errors v0.9.1
github.com/pkg/errors
# github.com/prometheus/procfs v0.6.0
github.com/prometheus/procfs
github.com/prometheus/procfs/internal/fs
github.com/prometheus/procfs/internal/util
# github.com/rs/zerolog v1.21.0
github.com/rs/zerolog
github.com/rs/zerolog/diode
github.com/rs/zerolog/diode/internal/diodes
github.com/rs/zerolog/internal/cbor
github.com/rs/zerolog/internal/json
github.com/rs/zerolog/log
# github.com/santhosh-tekuri/jsonschema v1.2.4
github.com/santhosh-tekuri/jsonschema
github.com/santhosh-tekuri/jsonschema/decoders
github.com/santhosh-tekuri/jsonschema/formats
github.com/santhosh-tekuri/jsonschema/loader
github.com/santhosh-tekuri/jsonschema/mediatypes
# github.com/sijms/go-ora v0.0.0-20210420114532-930751532d56
## explicit
github.com/sijms/go-ora
github.com/sijms/go-ora/advanced_nego
github.com/sijms/go-ora/converters
github.com/sijms/go-ora/network
github.com/sijms/go-ora/trace
# github.com/spf13/afero v1.6.0
github.com/spf13/afero
github.com/spf13/afero/mem
# github.com/spf13/cast v1.3.1
github.com/spf13/cast
# github.com/spf13/jwalterweatherman v1.1.0
github.com/spf13/jwalterweatherman
# github.com/spf13/pflag v1.0.5
github.com/spf13/pflag
# github.com/spf13/viper v1.7.1
## explicit
github.com/spf13/viper
# github.com/subosito/gotenv v1.2.0
github.com/subosito/gotenv
# github.com/xdg-go/pbkdf2 v1.0.0
github.com/xdg-go/pbkdf2
# github.com/xdg-go/scram v1.0.2
github.com/xdg-go/scram
# github.com/xdg-go/stringprep v1.0.2
github.com/xdg-go/stringprep
# github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a
github.com/youmark/pkcs8
# go.elastic.co/apm v1.11.0
go.elastic.co/apm
go.elastic.co/apm/apmconfig
go.elastic.co/apm/internal/apmcloudutil
go.elastic.co/apm/internal/apmcontext
go.elastic.co/apm/internal/apmhostutil
go.elastic.co/apm/internal/apmhttputil
go.elastic.co/apm/internal/apmlog
go.elastic.co/apm/internal/apmschema
go.elastic.co/apm/internal/apmstrings
go.elastic.co/apm/internal/apmversion
go.elastic.co/apm/internal/configutil
go.elastic.co/apm/internal/iochan
go.elastic.co/apm/internal/pkgerrorsutil
go.elastic.co/apm/internal/ringbuffer
go.elastic.co/apm/internal/wildcard
go.elastic.co/apm/model
go.elastic.co/apm/stacktrace
go.elastic.co/apm/transport
# go.elastic.co/apm/module/apmmongo v1.11.0
go.elastic.co/apm/module/apmmongo
# go.elastic.co/fastjson v1.1.0
go.elastic.co/fastjson
# go.mongodb.org/mongo-driver v1.5.1
## explicit
go.mongodb.org/mongo-driver/bson
go.mongodb.org/mongo-driver/bson/bsoncodec
go.mongodb.org/mongo-driver/bson/bsonoptions
go.mongodb.org/mongo-driver/bson/bsonrw
go.mongodb.org/mongo-driver/bson/bsontype
go.mongodb.org/mongo-driver/bson/primitive
go.mongodb.org/mongo-driver/event
go.mongodb.org/mongo-driver/internal
go.mongodb.org/mongo-driver/mongo
go.mongodb.org/mongo-driver/mongo/address
go.mongodb.org/mongo-driver/mongo/description
go.mongodb.org/mongo-driver/mongo/options
go.mongodb.org/mongo-driver/mongo/readconcern
go.mongodb.org/mongo-driver/mongo/readpref
go.mongodb.org/mongo-driver/mongo/writeconcern
go.mongodb.org/mongo-driver/tag
go.mongodb.org/mongo-driver/version
go.mongodb.org/mongo-driver/x/bsonx
go.mongodb.org/mongo-driver/x/bsonx/bsoncore
go.mongodb.org/mongo-driver/x/mongo/driver
go.mongodb.org/mongo-driver/x/mongo/driver/auth
go.mongodb.org/mongo-driver/x/mongo/driver/auth/internal/gssapi
go.mongodb.org/mongo-driver/x/mongo/driver/connstring
go.mongodb.org/mongo-driver/x/mongo/driver/dns
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt/options
go.mongodb.org/mongo-driver/x/mongo/driver/ocsp
go.mongodb.org/mongo-driver/x/mongo/driver/operation
go.mongodb.org/mongo-driver/x/mongo/driver/session
go.mongodb.org/mongo-driver/x/mongo/driver/topology
go.mongodb.org/mongo-driver/x/mongo/driver/uuid
go.mongodb.org/mongo-driver/x/mongo/driver/wiremessage
# golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
golang.org/x/crypto/ocsp
golang.org/x/crypto/pbkdf2
golang.org/x/crypto/scrypt
# golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
golang.org/x/sync/errgroup
golang.org/x/sync/semaphore
# golang.org/x/sys v0.0.0-20210414055047-fe65e336abe0
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
golang.org/x/sys/windows/registry
# golang.org/x/text v0.3.6
golang.org/x/text/transform
golang.org/x/text/unicode/norm
# gopkg.in/ini.v1 v1.62.0
gopkg.in/ini.v1
# gopkg.in/natefinch/lumberjack.v2 v2.0.0
## explicit
gopkg.in/natefinch/lumberjack.v2
# gopkg.in/yaml.v2 v2.4.0
gopkg.in/yaml.v2
# howett.net/plist v0.0.0-20201203080718-1454fab16a06
howett.net/plist
