module git.alfamikro.com/infra/sync-store-mro

go 1.16

require (
	git.alfamikro.com/go/toolkit v0.11.0
	github.com/sijms/go-ora v0.0.0-20210420114532-930751532d56
	github.com/spf13/viper v1.7.1
	go.mongodb.org/mongo-driver v1.5.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
