package sync

import (
	"context"
	"git.alfamikro.com/infra/sync-store-mro/internal/repository/mongo"
)

type (
	MongoRepository interface {
		GetDataFromMongo(ctx context.Context, role string) ([]mongo.ListOfStore, error)
	}

	OracleRepository interface{
		DeleteDataOracle(ctx context.Context) error
	 	InsertDataOracle(ctx context.Context, data string) error
	}
)
