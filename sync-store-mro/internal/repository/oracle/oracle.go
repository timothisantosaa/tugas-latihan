package oracle

import (
	"context"
	"database/sql"
	"git.alfamikro.com/go/toolkit/log"
)

type Repository struct {
	db *sql.DB
}

func NewOraclerepository(queryCtx context.Context, db *sql.DB) *Repository {
	return &Repository{
		db: db,
	}
}

const insertIntoTable = "INSERT INTO MRO_UNVACANT_STORE(STORE_CODE) VALUES(:1)"
const deleteFromTable = "TRUNCATE TABLE MRO_UNVACANT_STORE"

func (o *Repository) DeleteDataOracle(ctx context.Context) error{
	_, err := o.db.Exec(deleteFromTable)
	if err != nil{
		log.Fatal(err)
	}
	return nil
}

func (o *Repository) InsertDataOracle(ctx context.Context, data string) error{
	stmt, err := o.db.Prepare(insertIntoTable)
	if err != nil{
		log.Fatal(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(data)

	defer rows.Close()
	return nil
}

