package mongo

type OfficerV2 struct {
	Id string `bson:"_id"`
	Nik string `bson:"nik"`
	Nama string `bson:"nama"`
	Is_Firstman bool `bson:"is_firstman"`
	Active string `bson:"active"`
	Role_Code string `bson:"role_code"`
	Role string `bson:"role"`
	Role_Name string `bson:"role_name"`
	Role_Dept string `bson:"role_dept"`
	Role_Level int32 `bson:"role_level"`
	Dept string `bson:"dept"`
	Store_Name string `bson:"store_name"`
	Store_Code string `bson:"store_code"`
	Branch_Code string `bson:"branch_code"`
	List_Of_Store []ListOfStore `bson:"list_of_store"`
	Company string `bson:"company"`
	Is_Karyawan bool `bson:"is_karyawan"`
	Is_Acquisition bool `bson:"is_acquisition"`
	Is_Delivery bool `bson:"is_delivery"`
	Is_To bool `bson:"is_to"`
}

type ListOfStore struct {
	Branch_Code string `bson:"branch_code"`
	Store_Code string `bson:"store_code"`
	Store_Name string `bson:"store_name"`
	Officer_Count int64 `bson:"officer_count"`
	Location Loc `bson:"loc"`
	Kelurahan_Id int64 `bson:"kelurahan_id"`
	Kelurahan_Name string `bson:"kelurahan_name"`
}

type Loc struct {
	Latitude float64 `bson:"lat"`
	Longitude float64 `bson:"lon"`
}
