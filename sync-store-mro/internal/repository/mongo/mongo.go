package mongo

import (
	"context"
	"git.alfamikro.com/go/toolkit/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type collection string

var collectionAmaRegist collection = "officer_v2"

type Repository struct {
	db *mongo.Database
}

func NewMongoRepository(ctx context.Context, db *mongo.Database) *Repository {
	return &Repository{
		db: db,
	}
}

func (r *Repository) GetDataFromMongo(ctx context.Context, role string) ([]ListOfStore, error){
	var daftarToko OfficerV2
	var listOfStoreSlice []ListOfStore
	coll := r.db.Collection(string(collectionAmaRegist))
	filter := bson.M{"role" : role}

	curs, err := coll.Find(ctx, filter, options.Find())
	if err != nil{
		log.Fatal(err)
	}

	for curs.Next(ctx){
		if err := curs.Decode(&daftarToko); err != nil{
			log.Fatal(err)
		}
		listOfStoreSlice = append(listOfStoreSlice, daftarToko.List_Of_Store...)
	}
	return listOfStoreSlice, nil
}

