package process

import (
	"context"
	"git.alfamikro.com/go/toolkit/log"
	"git.alfamikro.com/infra/sync-store-mro/internal/repository/oracle"
	"git.alfamikro.com/infra/sync-store-mro/internal/sync"
)

type SyncProcess struct {
	mRepo sync.MongoRepository
	oRepo sync.OracleRepository
}

type Role string
var RoleMRO Role = "MRO"

func NewSyncProcess(mrepo sync.MongoRepository, orepo sync.OracleRepository) *SyncProcess {
	return &SyncProcess{
		mRepo: mrepo,
		oRepo: orepo,
	}
}

func removeDuplicateValues(inputSlice []string) []string {
	keys := make(map[string]bool)
	var list []string

	for _, entry := range inputSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func (s *SyncProcess) GetAndSyncDaftarToko(ctx context.Context) error{
	data, err := s.mRepo.GetDataFromMongo(ctx, string(RoleMRO))
	if err != nil{
		log.Fatal(err)
	}

	var strStoreCodeSlice []string
	for i := range data{
		strStoreCodeSlice = append(strStoreCodeSlice, data[i].Store_Code)
	}
	result := removeDuplicateValues(strStoreCodeSlice)

	if err := s.oRepo.DeleteDataOracle(ctx); err != nil{
		log.Fatal(err)
	}

	var MRO oracle.MROUnvacantStore
	for j := range result{
		MRO.Store_Code = result[j]
		if err := s.oRepo.InsertDataOracle(ctx, MRO.Store_Code); err != nil{
			log.Fatal(err)
		}
	}
	return nil
}

