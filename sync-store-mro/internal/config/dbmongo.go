package config

import (
	"git.alfamikro.com/go/toolkit/db/mongokit"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
)

func LoadDB(v *viper.Viper) (*mongo.Database, error) {
	return mongokit.NewFromViperFileConfig(v,"db.mongo")
}