package config

import (
	"git.alfamikro.com/go/toolkit/log"
	"git.alfamikro.com/go/toolkit/runtimekit"
	"git.alfamikro.com/go/toolkit/springcloud"
	"git.alfamikro.com/go/toolkit/web/httpclient"
	"github.com/spf13/viper"
)

func LoadConfig() *viper.Viper {
	appCtx, cancel := runtimekit.NewRuntimeContext()
	defer cancel()

	httpClient := httpclient.NewStdHTTPClient()
	configclient := springcloud.NewRemoteConfigClient(httpClient)
	v := viper.New()

	err := configclient.LoadViperConfig(appCtx, v)
	if err != nil {
		log.FromCtx(appCtx).Error(err, "load remote config failed")
	}
	return v
}