package config

import (
	"database/sql"
	"fmt"
	"git.alfamikro.com/go/toolkit/log"
	_ "github.com/sijms/go-ora" // use wrapped oracle driver
	"github.com/spf13/viper"
	"net/url"
	"time"
)

type connection struct {
	maxIdle           int
	maxLifetime       time.Duration
	keepAliveInternal time.Duration
	maxOpen           int
}

// DBConf ...
type DBConf struct {
	cfgName, username, password, host, schema, dbType string
	port                                              int
	connection
}

func (conf DBConf) getDBConnectionString() string {
	connURL := &url.URL{
		Scheme: conf.dbType,
		User:   url.UserPassword(conf.username, conf.password),
		Host:   fmt.Sprintf("%s:%d", conf.host, conf.port),
		Path:   conf.schema,
	}
	q := connURL.Query()
	q.Add("sslmode", "disable")
	connURL.RawQuery = q.Encode()

	return connURL.String()
}

func (conf DBConf) loadDB() *sql.DB {
	connectionString := conf.getDBConnectionString()

	newDB, err := sql.Open(conf.dbType, connectionString)
	if err != nil {
		log.Printf("error opening db connection error=%s db=$s", err, conf.cfgName)
	}

	newDB.SetMaxIdleConns(conf.maxIdle)
	newDB.SetConnMaxIdleTime(conf.maxLifetime)
	newDB.SetMaxOpenConns(conf.maxOpen)

	log.Println("successfully connected to db -", conf.cfgName)

	return newDB
}

// GetOracleDB ...
func GetOracleDB(viper *viper.Viper) DBConf {
	return getDBConfByName("oracle", viper)
}

func getDBConfByName(dbConfigName string, viper *viper.Viper) DBConf {
	db := DBConf{}
	db.cfgName = dbConfigName
	db.username = viper.GetString(fmt.Sprintf("db.%s.username", dbConfigName))
	db.password = viper.GetString(fmt.Sprintf("db.%s.password", dbConfigName))
	db.host = viper.GetString(fmt.Sprintf("db.%s.host", dbConfigName))
	db.port = viper.GetInt(fmt.Sprintf("db.%s.port", dbConfigName))
	db.schema = viper.GetString(fmt.Sprintf("db.%s.schema", dbConfigName))
	db.dbType = viper.GetString(fmt.Sprintf("db.%s.type", dbConfigName))

	db.connection.maxLifetime = viper.GetDuration(fmt.Sprintf("db.%s.conn.max-lifetime", dbConfigName))
	db.connection.keepAliveInternal = viper.GetDuration(fmt.Sprintf("db.%s.conn.keep-alive-interval", dbConfigName))
	db.connection.maxOpen = viper.GetInt(fmt.Sprintf("db.%s.conn.max-open", dbConfigName))
	db.connection.maxIdle = viper.GetInt(fmt.Sprintf("db.%s.conn.max-idle", dbConfigName))

	return db
}

// GetDB ...
func (conf DBConf) GetDB() *sql.DB {
	return conf.loadDB()
}

