package main

import (
	"context"
	"git.alfamikro.com/go/toolkit/log"
	"git.alfamikro.com/infra/sync-store-mro/internal/config"
	"git.alfamikro.com/infra/sync-store-mro/internal/process"
	"git.alfamikro.com/infra/sync-store-mro/internal/repository/mongo"
	"git.alfamikro.com/infra/sync-store-mro/internal/repository/oracle"
)

//remote config development : http://20.96.0.147:8888/sync-store-mro/dev
func main(){
	v := config.LoadConfig()

	mongoDB, err := config.LoadDB(v)
	if err != nil {
		log.Fatalf("error connecting DB: %s", err)
	}

	oracleDB := config.GetOracleDB(v).GetDB()
	if oracleDB == nil {
		log.Fatal("boot up failed")
	}

	mongoRepo := mongo.NewMongoRepository(context.Background(), mongoDB)
	oRepo := oracle.NewOraclerepository(context.Background(), oracleDB)
	syncProcces := process.NewSyncProcess(mongoRepo, oRepo)

	if err := syncProcces.GetAndSyncDaftarToko(context.Background()); err != nil{
		log.Fatal(err)
	}
}
